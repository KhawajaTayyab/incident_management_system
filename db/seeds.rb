# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
u = User.create(:first_name => "Admin", :last_name => "Admin", :email => "admin@scitechis.com", :password => "hellohello", :user_type => "Support")
u = User.create(:first_name => "Admin", :last_name => "Admin", :email => "admin@scitechis.com", :password => "hellohello", :user_type => "Support")
u.add_role :admin
puts "User With Admin Role and user_type is Support Created" 

index = 1
10.times do
	aUser = User.create(:first_name => "User#{index}", :last_name => "User#{index}", :email => "user#{index}@stis.pk", :password => "helloworld")
	puts "Creating User#{index} with Name: #{aUser.first_name} #{aUser.last_name}"
	index  = index + 1	
end

TicketType.create(:name => "Wifi")
TicketType.create(:name => "OS")
puts "Two Ticket Types created Wifi and OS"

TicketSubType.create(:name => "Internet Limited Access")
TicketSubType.create(:name => "Internet Speed Is Not Good")
TicketSubType.create(:name => "Window Os is Cruppted")

puts "Three Ticket Sub Types Created"

class CreateTicketAssignments < ActiveRecord::Migration
  def change
    create_table :ticket_assignments do |t|
    	t.belongs_to :user
    	t.belongs_to :ticket
      t.timestamps
    end
  end
end

class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
    	t.string :ticket_user_name
    	t.string :ticket_user_email
    	t.string :ticket_user_contact_number
    	t.string :ticket_title
    	t.string :ticket_description
      t.boolean :ticket_status,  :default => false, :null => false
      t.string :ticket_type
      t.string :ticket_sub_type
    	t.integer :creater_id
      t.integer :requester_id
      t.timestamps
    end
  end
end

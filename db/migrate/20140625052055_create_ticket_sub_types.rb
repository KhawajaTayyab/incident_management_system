class CreateTicketSubTypes < ActiveRecord::Migration
  def change
    create_table :ticket_sub_types do |t|
    	t.string :name, :unique => true
      t.timestamps
    end
  end
end

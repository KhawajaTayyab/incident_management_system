class CreateTicketTypes < ActiveRecord::Migration
  def change
    create_table :ticket_types do |t|
    	t.string :name, :unique => true
      t.timestamps
    end
  end
end

class TicketSubType < ActiveRecord::Base

	validates :name, uniqueness: true,  presence: true, length: {minimum: 2, maximum: 30}

end

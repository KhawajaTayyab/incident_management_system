class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

   has_many :created_tickets, dependent: :destroy, :class_name => "Ticket", foreign_key: 'creater_id'
   has_many :requested_tickets, dependent: :destroy, :class_name => "Ticket", foreign_key: 'requester_id'


   has_many :ticket_assignments, dependent: :destroy


   validates :contact_number, uniqueness: true, :allow_blank => true, length: {minimum: 11, maximum: 11}, numericality: { only_integer: true }
   validates :email, :presence => true, uniqueness: true
end

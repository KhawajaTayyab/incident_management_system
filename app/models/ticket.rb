class Ticket < ActiveRecord::Base

	belongs_to :requester, :class_name => "User"
	belongs_to :creater, :class_name => "User"

	has_many :ticket_assignments, dependent: :destroy

end

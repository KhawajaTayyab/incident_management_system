class ApplicationController < ActionController::Base
  
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.

	protect_from_forgery with: :exception

	before_action :configure_permitted_parameters, if: :devise_controller?


	def isAdmin
		if current_user.has_role? :isAdmin
			return true
		else
			return false
		end
	end



	####################################################
	####################################################
	####################################################
	#################PROTECTED METHODS##################
	####################################################
	####################################################
	####################################################


	protected

	def configure_permitted_parameters
	devise_parameter_sanitizer.for(:sign_up) << :first_name << :last_name
	devise_parameter_sanitizer.for(:account_update) << :first_name << :last_name << :contact_number
	end

end

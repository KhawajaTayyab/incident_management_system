class TicketsController < ApplicationController

	before_action :isAdmin, if: :user_signed_in?, only:[:newTicket]

	##  For Support User
	def newTicket
		@ticket = Ticket.new	
	end


	def createTicketInDirectly
		@ticket = Ticket.new(tickets_params)
		@ticket.creater_id = current_user.id
		binding.pry
		user = User.find_by_first_name(@ticket.ticket_user_name)
		binding.pry
		@ticket.requester_id = user.id
		binding.pry
		@ticket.ticket_status = true
		if @ticket.save
			flash[:success] = "Your Ticket is Generated"
			redirect_to root_path
		else
			flash[:alert] = "Error in your Ticket"
			render :new
		end
	end
	## For Support User


	## For Normal User
	def new
		@ticket = Ticket.new
	end

	def create
		@ticket = Ticket.new(tickets_params)
		@ticket.creater_id = current_user.id
		@ticket.requester_id = current_user.id
		binding.pry
		@ticket.ticket_status = true
		if @ticket.save
			flash[:success] = "Your Ticket is Generated"
			redirect_to root_path
		else
			flash[:alert] = "Error in your Ticket"
			render :new
		end
	end

	def index
		# See Ticket for Sngle User
		@allTickets = current_user.requested_tickets
	end

	def show
		@ticket = Ticket.find(params[:id])
	end

	def allTickets
		@allTickets = Ticket.all.order('id DESC')
	end

	
	private

	def tickets_params
		params.require(:ticket).permit(:ticket_user_name, :ticket_user_email, :ticket_user_contact_number, :ticket_title, :ticket_description, :ticket_type, :ticket_sub_type)
	end

end
